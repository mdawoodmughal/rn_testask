import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    Alert,
    Text,
    Keyboard
} from 'react-native';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Picker } from 'native-base';
import DatePicker from 'react-native-datepicker'

import { API, Authorization } from '../../utils/API'
import images from '../../assets/images'
import colors from '../../theme/colors';
import fonts from '../../assets/fonts';
import icons from '../../assets/icons';
import InputField from '../../components/InputField';

export default class MedicationScreen extends Component {

    constructor(props) {
        super(props)

        this.state = {
            loading: false,
            isCheckedMedication: false,
            name: '',
            amount: '',
            unit: ['g', 'mg'],
            selectedUnit: 'g',
            number: '',
            interval: ['Daily', 'Weekly', 'Monthly'],
            selectedInterval: 'Daily',
            duration: null,
            additionalInfo: '',
            showDatePicker: false,
        }
    }

    callBasicAuthAPI = async () => {
        Keyboard.dismiss()
        try {
            const username = 'postman'
            const password = 'password'
            const URL = API.BASIC_AUTH + '?Username=' + username + '&Password' + password
            this.setState({ loading: true })
            const response = await fetch(URL, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': Authorization
                }
            })
            this.setState({ loading: false })
            console.log('MedicationScreen', 'callBasicAuthAPI-response-status', response.status)
            if (response.status == 200) {
                Alert.alert(null, "You are authorized")
            } else {
                Alert.alert(null, "You are not authorized")
            }
        } catch (error) {
            console.log('MedicationScreen', 'callBasicAuthAPI-error', error)
        }
    }

    render() {
        const {
            loading,
            isCheckedMedication,
            name,
            amount,
            unit,
            selectedUnit,
            number,
            interval,
            selectedInterval,
            duration,
            additionalInfo,
            showDatePicker
        } = this.state
        return (
            <View style={styles.container}>
                <KeyboardAwareScrollView
                    innerRef={ref => {
                        this.scrollRef = ref
                    }}
                    keyboardShouldPersistTaps={'handled'}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{
                        flexGrow: 1,
                    }}
                    bounces={false}
                    extraHeight={150}>
                    <View style={[styles.headerContainerStyle]}>
                        <TouchableOpacity
                            style={styles.headerBackIconContianerStyle}
                            onPress={() => {
                                Alert.alert(null, "No navigation added here")
                            }}>
                            <Image
                                style={styles.headerBackIconStyle}
                                source={icons.iconBack}
                            />
                        </TouchableOpacity>
                        <Image
                            style={styles.headerImageStyle}
                            source={images.imageLinePattern}
                        />
                        <View style={styles.headerTextContainerStyle}>
                            <Text style={styles.headerTextStyle}>{'Do you take any Medications?'}</Text>
                        </View>
                    </View>
                    <View style={styles.contentContainerStyle}>
                        <View style={[styles.checkBoxContainerStyle, { marginTop: -46 }]}>
                            <TouchableOpacity
                                style={[styles.checkBoxStyle, { borderColor: isCheckedMedication ? colors.primary : colors.grey }]}
                                onPress={() => {
                                    this.setState({ isCheckedMedication: !isCheckedMedication })
                                }}>
                                {isCheckedMedication &&
                                    <Image
                                        style={styles.checkIconStyle}
                                        source={icons.iconCheck}
                                    />
                                }
                            </TouchableOpacity>
                            <View style={styles.checkBoxTitleContainerStyle}>
                                <Text style={styles.textStyle}>{'No, I don’t take any medications'}</Text>
                            </View>
                        </View>
                        <View style={styles.medicationDetailsContainerStyle}>
                            <InputField
                                value={name}
                                label='Medication Name'
                                placeholder='Type your Medication Here'
                                onTextChange={(text) => {
                                    this.setState({ name: text })
                                }}
                            />
                            <View style={styles.rowComContainerStyle}>
                                <InputField
                                    value={amount}
                                    label='Amount'
                                    placeholder='Type…'
                                    keyboardType={'numeric'}
                                    onTextChange={(text) => {
                                        this.setState({ amount: text })
                                    }}
                                    containerStyle={{ width: '45%' }}
                                />
                                <View style={{ width: '50%', marginBottom: 10, justifyContent: 'center' }}>
                                    <Picker
                                        mode="dropdown"
                                        style={styles.pickerStyle}
                                        selectedValue={selectedUnit}
                                        iosHeader={"Select Unit"}
                                        placeholder={"Select Unit"}
                                        onValueChange={(item, index) => {
                                            this.setState({ selectedUnit: item })
                                        }}>
                                        {unit.map((item, index) => {
                                            return <Picker.Item key={index} label={item} value={index} />
                                        })}
                                    </Picker>
                                    <Image
                                        style={[styles.checkIconStyle, { tintColor: colors.placeHolder, position: 'absolute', right: 10 }]}
                                        source={icons.iconDown}
                                    />
                                </View>
                            </View>
                            <View style={styles.rowComContainerStyle}>
                                <InputField
                                    value={number}
                                    label='Number'
                                    placeholder='2'
                                    keyboardType={'numeric'}
                                    onTextChange={(text) => {
                                        this.setState({ number: text })
                                    }}

                                    containerStyle={{ width: '45%' }}
                                />
                                <View style={{ width: '50%', marginBottom: 10, justifyContent: 'center' }}>
                                    <Picker
                                        mode="dropdown"
                                        style={styles.pickerStyle}
                                        selectedValue={selectedInterval}
                                        iosHeader={"Select Interval"}
                                        placeholder={"Select Interval"}
                                        onValueChange={(item, index) => {
                                            console.log('item', item)
                                            this.setState({ selectedInterval: item })
                                        }}>
                                        {interval.map((item, index) => {
                                            return <Picker.Item key={index} label={item} value={index} />
                                        })}
                                    </Picker>
                                    <Image
                                        style={[styles.checkIconStyle, { tintColor: colors.placeHolder, position: 'absolute', right: 10 }]}
                                        source={icons.iconDown}
                                    />
                                </View>
                            </View>
                            <View style={{ width: '100%', marginVertical: 10 }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Text style={styles.labelStyle}>{'Duration'}</Text>
                                    <Image
                                        style={[styles.checkIconStyle, { tintColor: colors.placeHolder, marginLeft: 10 }]}
                                        source={icons.iconInfo}
                                    />
                                </View>
                                <TouchableOpacity
                                    style={[styles.pickerStyle, { justifyContent: 'center' }]}
                                    onPress={() => {
                                        if (this.datePicker) this.datePicker.onPressDate()
                                    }}>
                                    <DatePicker
                                        ref={ref => this.datePicker = ref}
                                        style={{ width: 200 }}
                                        date={this.state.date}
                                        mode="date"
                                        placeholder="select date"
                                        format="YYYY-MM-DD"
                                        minDate="2016-05-01"
                                        maxDate="2050-06-01"
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        hideText={true}
                                        showIcon={false}
                                        customStyles={{}}
                                        onDateChange={(date) => { this.setState({ duration: date }) }}
                                    />
                                    <View style={{ position: 'absolute', flexDirection: 'row', alignItems: 'center' }}>
                                        <View style={{ flex: 1, paddingHorizontal: 10 }}>
                                            <Text style={[styles.labelStyle, { color: duration ? colors.black : colors.placeHolder }]}>{duration ? duration : "Select Date"}</Text>
                                        </View>
                                        <Image
                                            style={[styles.checkIconStyle, { tintColor: colors.placeHolder, marginHorizontal: 10 }]}
                                            source={icons.iconDown}
                                        />
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <InputField
                                value={additionalInfo}
                                showInfoIcon={true}
                                label='Additional Information'
                                placeholder='Write more information'
                                multiline={true}
                                onTextChange={(text) => {
                                    this.setState({ additionalInfo: text })
                                }}
                                inputStyle={{ width: '100%', height: 100 }}
                            />
                        </View>
                        <TouchableOpacity
                            activeOpacity={0.5}
                            style={[styles.buttonStyle, { height: 56, backgroundColor: colors.white, flexDirection: 'row' }]}
                            onPress={() => {
                                this.callBasicAuthAPI()
                            }}>
                            <Image
                                style={[styles.checkIconStyle, { marginRight: 10 }]}
                                source={icons.iconAdd}
                            />
                            <Text style={[styles.buttonTextStyle, { color: colors.primary }]}>{'Add Medication'}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            activeOpacity={0.5}
                            style={styles.buttonStyle}
                            onPress={() => {
                                this.callBasicAuthAPI()
                            }}>
                            <Text style={styles.buttonTextStyle}>{'Next'}</Text>
                        </TouchableOpacity>
                    </View>
                </KeyboardAwareScrollView>
                {loading &&
                    <View style={styles.loadingContainer}>
                        <Image
                            style={{ width: 80, height: 80, resizeMode: 'contain' }}
                            source={images.loadingGif}
                        />
                    </View>
                }
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    headerContainerStyle: {
        width: '100%',
        height: 330,
        backgroundColor: colors.primary
    },
    headerImageStyle: {
        width: '60%',
        height: '60%',
        resizeMode: 'stretch',
        position: 'absolute',
        top: 0,
        right: 0,
    },
    headerBackIconContianerStyle: {
        marginTop: 40,
        padding: 10,
        zIndex: 999
    },
    headerBackIconStyle: {
        width: 25,
        height: 25,
        resizeMode: 'contain',
    },
    headerTextContainerStyle: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    },
    headerTextStyle: {
        fontSize: 18,
        lineHeight: 24,
        color: colors.white,
        fontFamily: fonts.Bold
    },
    contentContainerStyle: {
        flex: 1,
        backgroundColor: colors.background,
        borderTopRightRadius: 14,
        borderTopLeftRadius: 14,
        marginTop: -20,
        padding: 20,
        paddingBottom: 40,
    },
    checkBoxContainerStyle: {
        width: '100%',
        borderRadius: 10,
        height: 56,
        backgroundColor: colors.white,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 20,
    },
    checkBoxStyle: {
        width: 22,
        height: 22,
        borderRadius: 6,
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    checkIconStyle: {
        width: 15,
        height: 15,
        resizeMode: 'contain',
        tintColor: colors.primary
    },
    textStyle: {
        fontSize: 15,
        color: colors.blackText,
        fontFamily: fonts.Medium
    },
    checkBoxTitleContainerStyle: {
        flex: 1,
        paddingHorizontal: 20
    },
    medicationDetailsContainerStyle: {
        width: '100%',
        borderRadius: 10,
        backgroundColor: colors.white,
        padding: 20,
        paddingVertical: 10,
        marginVertical: 10,
    },
    buttonStyle: {
        backgroundColor: colors.red,
        width: '100%',
        height: 48,
        marginVertical: 10,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonTextStyle: {
        fontSize: 16,
        color: colors.white,
        fontFamily: fonts.Bold,
    },
    rowComContainerStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
    },
    labelStyle: {
        marginVertical: 10,
        fontSize: 15,
        fontFamily: fonts.Medium
    },
    pickerStyle: {
        width: '100%',
        height: 48,
        backgroundColor: colors.inputBackground,
        borderRadius: 10,
    },
    loadingContainer: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.transparentGrey
    }
})