export default {
  background: '#e7e7eb',
  white: 'white',
  primary: '#1d9d9e',
  black: 'black',
  blackText: '#222222',
  placeHolder: '#b5b5b6',
  transparentGrey: '#f2f2f2AA',
  grey: '#525252',
  inputBackground: '#e7e7eb',
  red: '#eb2025',
  blue: '#1d9d9e'
}
