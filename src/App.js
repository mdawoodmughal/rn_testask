import React, { Component } from 'react';
import { View, StatusBar } from 'react-native'

import Routing from './Routing'

console.disableYellowBox = true

export default class App extends Component {
  render() {
    return (
      <View style={{ flex: 1, }} >
        <StatusBar backgroundColor="#00000000" barStyle="light-content" />
        <Routing />
      </View >
    );
  }
}