const BASE_URL = 'https://postman-echo.com'

const API_URL = BASE_URL

export const Authorization = 'Basic cG9zdG1hbjpwYXNzd29yZA=='

export const API = {
    BASIC_AUTH: API_URL + '/basic-auth'
}