export default {
    Bold: "HKGrotesk-Bold",
    Medium: "HKGrotesk-Medium",
    SemiBold: "HKGrotesk-SemiBold"
}