export default {
    iconAdd: require('./icon_add.png'),
    iconBack: require('./icon_back.png'),
    iconCheck: require('./icon_check.png'),
    iconDown: require('./icon_down.png'),
    iconInfo: require('./icon_info.png'),
}