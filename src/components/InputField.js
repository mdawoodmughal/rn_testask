import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TextInput,
    Image
} from 'react-native';
import fonts from '../assets/fonts';
import icons from '../assets/icons';
import colors from '../theme/colors';

export default InputField = (props) => {

    return (
        <View style={[styles.container, props.containerStyle]} >
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Text style={[styles.labelStyle, props.labelStyle]}>{props.label}</Text>
                {props.showInfoIcon &&
                    <Image
                        style={[styles.infoIconStyle]}
                        source={icons.iconInfo}
                    />
                }
            </View>
            <TextInput
                value={props.value}
                autoCapitalize={props.autoCapitalize ? props.autoCapitalize : 'sentences'}
                blurOnSubmit={props.blurOnSubmit ? props.blurOnSubmit : false}
                onChangeText={(text) => {
                    props.onTextChange(text)
                }}
                selectionColor={colors.primary}
                style={[styles.textInputStyle, props.inputStyle]}
                multiline={props.multiline ? props.multiline : false}
                keyboardType={props.keyboardType}
                textContentType={props.textContentType}
                placeholder={props.placeholder}
                placeholderTextColor={props.placeholderTextColor ? props.placeholderTextColor : colors.placeHolder}
                onFocus={(event: Event) => {
                    if (props.onFocus) {
                        props.onFocus(event)
                    }
                }}
                maxLength={props.maxLength}
            />
        </View >
    )
}


const styles = StyleSheet.create({
    container: {
        width: '100%',
        paddingVertical: 10,
    },
    textInputStyle: {
        width: '100%',
        height: 48,
        borderRadius: 10,
        backgroundColor: colors.inputBackground,
        paddingHorizontal: 10,
    },
    labelStyle: {
        marginVertical: 10,
        fontSize: 15,
        fontFamily: fonts.Medium
    },
    infoIconStyle: {
        width: 15,
        height: 15,
        resizeMode: 'contain',
        tintColor: colors.primary,
        tintColor: colors.placeHolder, marginLeft: 10
    },
})